<?php

require_once "../vendor/autoload.php";


# Configure website
setlocale(LC_CTYPE, 'fr_FR');
date_default_timezone_set('Europe/Paris');


# Handle session and check that user is logged in
$req = $_GET['route'];

@session_start();

$user = $_SESSION['user']['name'] ?? null;
if (!$user && $req != 'login.php')
{
	header('Location: /login.php?url='.urlencode($_SERVER['REQUEST_URI']??'/'));
	die();
}


# Load route
$file = $req == "" ? "index.php" : $req;

try {
	require_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . basename($file);
}
catch(\Exception $e)
{
	die($e->getMessage());
}
