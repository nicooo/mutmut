<?php

\Mutmut\Money::HandlePaymentAction('/payments.php');

\Mutmut\Twig::Display('payments.html', ['payments_mine','payments_others']);
