<?php

@session_start();

// Log out
if (isset($_GET['out']))
{
	unset($_SESSION);
	session_destroy();
	header('Location: /login.php');
	die();
}


// Logged in, nothing to do here: redirect to '/'
if ($_SESSION['user']['name'] ?? false)
{
	header('Location: /');
	die();
}


// Try to login
$login    = $_POST['login']    ?? null;
$password = $_POST['password'] ?? null;

if ($login && $password)
{
	$user = \Mutmut\Users::Login($login, $password);
	if ($user)
	{
		$_SESSION['user'] = $user;
		header('Location: '.($_POST['redirect'] ?: '/'));
		die();
	}
	die("Erreur d'authentification");
}


\Mutmut\Twig::Display('login.html');
