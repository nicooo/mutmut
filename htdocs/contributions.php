<?php

$action = $_POST['action'] ?? null;
$confirm = $_POST['confirm'] ?? false;
if ($action == 'close' && $confirm)
{
	\Mutmut\Money::CloseContributions();
}

if ($action == 'declare-for-user')
{
	if ($_SESSION['user']['role'] != 'accountant')
		die('Only accountants can declare for somebody else');

	\Mutmut\Money::DeclareContribution($_POST['user_name'], $_POST['contribution']);

	header('Location: /contributions.php');
	die();
}

\Mutmut\Twig::Display('contributions.html', ['contributions', 'who_didnt_declare', 'post']);
