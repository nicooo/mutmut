<?php

\Mutmut\Money::HandlePaymentAction('/redistribution.php');

\Mutmut\Twig::Display('redistribution.html', ['equilibrium','payments_redistribution','redistribution_messages','contributions','needs']);
