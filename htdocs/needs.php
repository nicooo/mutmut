<?php

\Mutmut\Money::HandlePaymentAction('/needs.php');
\Mutmut\Money::HandleNeedAction('/needs.php');

\Mutmut\Twig::Display('needs.html', ['needs', 'registers', 'contributions', 'payments_redistribution']);
