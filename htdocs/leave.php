<?php

\Mutmut\Money::HandlePaymentAction("/leave.php?userid={$_GET['userid']}");

@session_start();

if (!$_GET['userid'] || $_SESSION['user']['role'] != 'accountant')
{
	header("Location: /");
	die();
}

$user = \Mutmut\Users::GetByID($_GET['userid']);
if ($user['until'])
	die("{$user['name']} already left");


$userid = $_POST['userid'] ?? null;
if ($userid)
{
	\Mutmut\Users::UserLeaves($userid);
	header("Location: /");
	die();
}


\Mutmut\Twig::Display('leave.html', ['get']);
