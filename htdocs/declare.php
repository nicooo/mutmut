<?php

@session_start();

if (!\Mutmut\Money::ContributionEditable($_SESSION['user']['name']))
{
	header('Location: /');
	die();
}

$calc_contrib = [];
if (isset($_POST['income']))
{
	$_POST['income'] = (int)$_POST['income'];
	$_POST['rent']   = (int)$_POST['rent'];
	$contrib = round(($_POST['income'] - $_POST['rent']) * 0.1);
	$calc_contrib = ['user_contrib'=>['edit_contrib'=>['amount'=>$contrib]]];
}

$contribution = $_POST['contribution'] ?? null;
if (!is_null($contribution))
{
	\Mutmut\Money::DeclareContribution($_SESSION['user']['name'], $contribution);
	
	header('Location: /contributions.php');
	die();
}


\Mutmut\Twig::Display('declare.html', ['contributions','post'], $calc_contrib);
