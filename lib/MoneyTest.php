<?php declare(strict_types=1);

namespace Mutmut;

final class MoneyTest extends TestWithDB
{
	public function testJoiningUserCantHaveAntedatedNeedPayment()
	{
		$adm = Users::GetID('admin');

		$this->expectException(\Exception::class);
		Money::CreatePayment($adm, $adm, 100, 'cash', 'need');
	}

	public function testStartWithRegisters()
	{
		Dates::SetNow('2022-01-13');

		// create user
		Users::Create('bob', '', 'passbob1');

		$adm = Users::GetID('admin');
		$bob = Users::GetID('bob');

		$registers = Money::GetRegisters();
		$this->assertRoundedEquals($registers[0]['register'], 0);
		$this->assertRoundedEquals($registers[1]['register'], 0);

		Money::SetRegister($adm, 'last', 1000);
		Money::SetRegister($bob, 'last', 1000);

		$this->assertRoundedEquals(Money::GetRegister(), 2000);

		$registers = Money::GetRegisters();
		$this->assertRoundedEquals($registers[0]['register'], 1000);
		$this->assertRoundedEquals($registers[1]['register'], 1000);

		// contributions
		Dates::SetNow('2022-01-14'); Money::DeclareContribution('admin', 150);
		Dates::SetNow('2022-01-15'); Money::DeclareContribution('bob', 50);

		// balance money
		Dates::SetNow('2022-01-16');
		Money::CreatePayment($adm, $bob, 100, 'cash', 'redistribution');

		$registers = Money::GetRegisters();
		$this->assertRoundedEquals($registers[0]['register'], 1050);
		$this->assertRoundedEquals($registers[1]['register'], 1050);
	}

	public function testNeedsForOutOfRedistributionUser()
	{
		Dates::SetNow('2022-01-03');

		// create users
		Users::Create('bob', '', 'passbob1');
		Users::Create('camelia', '', 'passcam5');

		$adm = Users::GetID('admin');
		$bob = Users::GetID('bob');
		$cam = Users::GetID('camelia');

		// contributions
		Dates::SetNow('2022-01-04'); Money::DeclareContribution('admin', 120);
		Dates::SetNow('2022-01-05'); Money::DeclareContribution('bob', 80);
		Money::CloseContributions();  # Camelia doesn't take part in the redistribution this month

		// redistribution
		Dates::SetNow('2022-01-06');
		Money::CreatePayment($adm, $bob, 20, 'cash', 'redistribution');

		Dates::SetNow('2022-01-07');
		$this->assertRoundedEquals(Money::GetRegister(['username' => 'camelia']), 0);
		Money::AddNeed('camelia', 50); $need_id = 1;
		Money::CreatePayment($cam, $cam, 50, 'cash', 'need', $need_id);

		$this->assertRoundedEquals(Money::GetRegister(['username' => 'camelia']), -50);
	}
}
