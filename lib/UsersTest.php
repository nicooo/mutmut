<?php declare(strict_types=1);

namespace Mutmut;

final class UsersTest extends TestWithDB
{
	public function testGlobalRegisterWhenUsersLeave()
	{
		Dates::SetNow(strtotime('2020-01-01'));

		Users::Create('bob', '', 'passbob1');
		$adm = Users::GetID('admin');
		$bob = Users::GetID('bob');

		Money::DeclareContribution('admin', 150);
		Money::DeclareContribution('bob', 50);
		Money::CreatePayment($adm, $bob, 100, 'cash', 'redistribution');

		$registers = Money::GetRegisters();
		$this->assertRoundedEquals($registers[0]['register'], 50);
		$this->assertRoundedEquals($registers[1]['register'], 50);

		Dates::SetNow(strtotime('2020-02-01'));

		Users::UserLeaves($adm);

		$this->assertEquals(Money::GetRegister(), 50, "Both admin and bob had 50 in register, but admin left. There should only be 50 left in global register");
	}

	public function testUserMasto()
	{
		Users::Create('bob', 'they', 'p@s5W°rd', $masto = '@user@instance.org');
		$id = 2;  # ID 1 is admin

		$this->assertEquals(Users::GetByID($id)['masto'],     $masto);
		$this->assertEquals(Users::GetByName('bob')['masto'], $masto);
		$this->assertEquals(Users::GetAll()[1]['masto'],      $masto);  # 0th is admin

		Users::Edit('bob', 'they', 'p@s5W°rd', $masto = '@BOB@instance.org', $id);

		$this->assertEquals(Users::GetByID($id)['masto'],     $masto);
	}
}
