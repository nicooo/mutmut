<?php

namespace Mutmut;

class Cacheable
{
	private static $cache = [];
	
	protected static function empty_cache()
	{
		self::$cache = [];
	}
	
	protected static function get_cache($ckey)
	{
		return self::$cache[$ckey] ?? null;
	}
	
	protected static function set_cache($ckey, $value)
	{
		return self::$cache[$ckey] = $value;
	}
	
	protected static function ckey($funcname, $params)
	{
		ksort($params);
		return $funcname.'-'.http_build_query($params);
	}
}
