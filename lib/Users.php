<?php

namespace Mutmut;

class Users extends Cacheable
{
	public static function GetID (string $username): int
	{
		$ckey = self::ckey(__FUNCTION__, ['username' => $username]);
		$cached = self::get_cache($ckey);
		if (!is_null($cached))
			return $cached;

		$res = DB::Query('SELECT rowid FROM users WHERE name = :name', [':name' => $username]);
		if (count($res) == 0)
			throw new \Exception("Unknown user '$username'");

		return self::set_cache($ckey, $res[0]['rowid']);
	}

	public static function GetByID(int $id)
	{
		$user = DB::Query('SELECT rowid, name, pronoun, masto, role, since, until FROM users WHERE rowid = :rowid', [':rowid' => $id]);
		if (count($user) != 1)
			throw new \Exception("Couldn't find user with ID '$id'");

		return $user[0];
	}

	public static function GetByName($username)
	{
		$user = DB::Query('SELECT rowid, name, pronoun, masto, role, since, until FROM users WHERE name = :name', [':name' => $username]);
		if (count($user) != 1)
			throw new \Exception("Couldn't find user '$username'");

		return $user[0];
	}

	public static function LowerAsciiName($name)
	{
		return strtolower(iconv("UTF-8", "ASCII//TRANSLIT", $name));
	}

	public static function GetAll ($options=[])
	{
		$ckey = self::ckey(__FUNCTION__, $options);
		$cached = self::get_cache($ckey);
		if (!is_null($cached))
			return $cached;

		$options = array_merge(['time' => Dates::Now()], $options);
		$options['time'] = strtotime(date('Y-m-01 00:00:00', $options['time']));

		$values = [];

		$since = $options['since'] ?? false;
		$since_clause = '';
		if ($since)
		{
			if (preg_match('/^\d{4}-\d{2}$/',$since))
				$since = strtotime("$since-01");
			$since_clause = 'since >= :date1 AND since < :date2';
			$values = array_merge($values,[
				':date1' => $since,
				':date2' => strtotime(date('Y-m-01 00:00:00',$since) . ' +1 month'),
			]);
		}
		else {
			$since_clause = 'since <= :date';
			$values[':date'] = $options['time'];
		}

		$until = $options['until'] ?? false;
		$until_clause = '';
		if ($until)
		{
			if (preg_match('/^\d{4}-\d{2}$/',$until))
				$until = strtotime("$until-01");
				$until_clause = '(until >= :date1 AND until < :date2 OR until IS NULL)';
				$values = array_merge($values,[
					':date1' => $until,
					':date2' => strtotime(date('Y-m-01 00:00:00',$until) . ' +1 month'),
				]);
		}
		else {
			$until_clause = '(until >= :date OR until IS NULL)';
			$values[':date'] = $options['time'];
		}

		$users = DB::Query('
			SELECT rowid, name, pronoun, masto, role, since, until
			FROM users
			WHERE ' . join(' AND ', array_filter([$since_clause,$until_clause])),
			$values
		);

		usort($users, fn($u1, $u2) => strcmp(self::LowerAsciiName($u1['name']), self::LowerAsciiName($u2['name'])));

		return self::set_cache($ckey, $users);
	}

	public static function Login ($login,$password)
	{
		$rows = DB::Query('
			SELECT rowid AS id, name, pronoun, masto, role, since
			FROM users
			WHERE name = :name
			  AND password = :password
		  ',
			[
				':name'     => $login,
				':password' => sha1($password),
			]
		);
		return count($rows) != 1 ? null : $rows[0];
	}

	public static function Create ($name, $pronoun, $password, $masto='')
	{
		$redistr = Money::GetPayments(['reason'=>'redistribution']);
		$since = count($redistr) > 0
			? Dates::FirstDayOfMonthMidnight('next')
			: Dates::FirstDayOfMonthMidnight('this');

		self::Edit($name, $pronoun, $password, $masto, null, $since);
	}

	public static function Edit ($name, $pronoun, $password, $masto, $id='logged-in', $since=null)
	{
		self::empty_cache();

		@session_start();

		if ($id == 'logged-in')
			$id = $_SESSION['user']['id'];

		$params = [
			':name' => $name,
			':pronoun' => $pronoun,
			':masto' => $masto,
		];
		if ($id)
			$params[':id'] = $id;
		else
			$params[':since'] = $since;

		$pwd_sql = '';
		if ($password)
		{
			if (mb_strlen($password) < 8)
				throw new \Exception('Password too short');

			$pwd_sql = ', password = :password';
			$params[':password'] = sha1($password);
		}
		else if (!$id)
		{
			throw new \Exception("Can't create user without password");
		}

		DB::Query(
			$id
			? "
				UPDATE users
				SET
					name    = :name,
					pronoun = :pronoun,
					masto   = :masto
					$pwd_sql
				WHERE
					rowid = :id
			"
			: "
				INSERT INTO users (name, pronoun, password, masto, since, role) VALUES (:name, :pronoun, :password, :masto, :since, 'member');
			",
			$params
		);

		// update session
		if ($id)
		{
			$_SESSION['user']['name']    = $name;
			$_SESSION['user']['pronoun'] = $pronoun;
			$_SESSION['user']['masto']   = $masto;
		}
	}

	public static function GiveAccountantRole ($new_accountant)
	{
		self::empty_cache();

		if (!is_null($new_accountant))
		{
			$curr_role = DB::Query("SELECT role FROM users WHERE rowid = :id", [':id' => $new_accountant])[0]['role'];
			if ($curr_role == 'accountant')
				throw new \Exception("already an accountant");
		}

		@session_start();

		// revoke logged in user accountant role
		DB::Query("UPDATE users SET role = 'member' WHERE rowid = :id", [':id' => $_SESSION['user']['id']]);
		$_SESSION['user']['role'] = 'member';

		// give accountant role to other user (has to logout and in again)
		if (!is_null($new_accountant))
			DB::Query("UPDATE users SET role = 'accountant' WHERE rowid = :id", [':id' => $new_accountant]);
	}

	public static function UserLeaves ($userid)
	{
		$enddate = Dates::LastDayOfMonth235959('last');
		DB::Query(
			"UPDATE users SET until = :enddate WHERE rowid = :userid",
			[
				':enddate' => $enddate,
				':userid' => $userid,
			]
		);
	}
}
