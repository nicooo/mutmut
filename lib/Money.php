<?php

namespace Mutmut;

class Money extends Cacheable
{
	const CONTRIB_NONE = 1;
	const CONTRIB_EDITABLE = 2;
	const CONTRIB_NEXTMONTH = 3;
	const CONTRIB_NOTUSERYET = 4;

	public static function SetRegister($userid, $date, $amount)
	{
		self::empty_cache();

		DB::Query(
			'INSERT INTO registers (user, date, amount) VALUES (:user, :date, :amount)',
			[
				':user' => $userid,
				':date' => Dates::LastDayOfMonth235959($date),
				':amount' => $amount,
			]
		);
	}

	public static function DeclareContribution ($username, $amount = INF, $participates = true)
	{
		self::empty_cache();

		if ($participates && $amount == INF)
			throw new \Exception("Actual contributions (participates === true) must have an actual amount (given: $amount)");

		if ($amount !== '0' && (float)$amount == 0)  // amount must be an integer; 0 is also valid
			die('Revenu incorrect');

		@session_start();
		$editable = self::ContributionEditable($username);
		$contrib_time = Dates::Now();
		switch ($editable) {
			case self::CONTRIB_NONE:
			case self::CONTRIB_EDITABLE:
				// cases are okay and variable `$contrib_time = Dates::Now()` is correct
				break;
			case self::CONTRIB_NEXTMONTH:
				$contrib_time = Dates::FirstDayOfMonthMidnight('next');
				break;
			default:
				throw new \Exception("Can't declare or edit declaration");
		}

		$delete_after = strtotime(date('Y-m-01',$contrib_time));

		$userid = Users::GetID($username);
		DB::Query(
			'DELETE FROM contribute WHERE date >= :thismonth AND user_from = :user_from',
			[
				':thismonth' => $delete_after,
				':user_from' => $userid,
			]
		);
		DB::Query(
			'INSERT INTO contribute (date, amount, user_from, participates) VALUES (:d, :a, :f, :p)',
			[
				':d' => $contrib_time,
				':a' => $amount == INF ? -1 : round((float)$amount),
				':f' => $userid,
				':p' => $participates ? 1 : 0,
			]
		);
	}

	public static function GetContributions ($options=[])
	{
		$ckey = self::ckey(__FUNCTION__, $options);
		$cached = self::get_cache($ckey);
		if (!is_null($cached))
			return $cached;

		$options = array_merge(['time' => Dates::Now()], $options);
		$i = Dates::GetInterval($options['time']);

		$query = '
			SELECT u.rowid AS userid, name, amount, date, participates
			FROM contribute c
			JOIN users u ON u.rowid = c.user_from
			WHERE date >= :thatmonth
				AND date <  :nextmonth
			';
		$values = [
			':thatmonth' => $i['thatmonth'],
			':nextmonth' => $i['nextmonth'],
		];

		if ($options['username'] ?? false)
			$options['userid'] = Users::GetID($options['username']);

		if ($options['userid'] ?? false)
		{
			$query .= ' AND user_from = :user_from ';
			$values[':user_from'] = $options['userid'];
		}

		if (isset($options['participates']))
			$query .= $options['participates'] ? ' AND participates ' : 'AND NOT participates';

		$query .= 'ORDER BY date DESC';

		return self::set_cache($ckey, array_map(
			fn($c) => array_merge($c,[
			'editable' => $c['date'] >= strtotime(date('Y-m-01',Dates::Now())) && (int)date('d',Dates::Now()) <= 5
			|| $c['date'] >= Dates::FirstDayOfMonthMidnight('next'),
			]),
			DB::Query($query,$values)));
	}

	public static function GetContributionsTotals ($options=[])
	{
		$contrib = self::GetContributions(array_merge($options, ['participates' => true]));
		$sum_contrib = array_sum(array_map(fn($c) =>$c['amount'],$contrib));
		$avg_contrib = count($contrib) > 0 ? $sum_contrib / count($contrib) : 0;

		return [
			'sum_contrib' => $sum_contrib,
			'avg_contrib' => $avg_contrib,
			'sum_contrib_redistr' => $sum_contrib/2,
			'avg_contrib_redistr' => $avg_contrib/2,
		];
	}

	public static function ContributionEditable ($username)
	{
		$user = Users::GetByName($username);
		if ($user['since'] > Dates::Now())
			return self::CONTRIB_NOTUSERYET;

		$contribution = self::GetContributions(['username'=>$username]);

		if (count($contribution) == 0)
			return self::CONTRIB_NONE;

		if ($contribution[0]['editable'])
			return self::CONTRIB_EDITABLE;

		if ((int)date('d',Dates::Now()) >= 25)
			return self::CONTRIB_NEXTMONTH;

		return 0;
	}

	public static function WhoDidntDeclare ()
	{
		$name = fn($r) => $r['name'];
		$users = array_map($name, Users::GetAll());
		$declared = array_map($name, self::GetContributions());

		return array_diff($users,$declared);
	}

	public static function GetNeeds ($options=[])
	{
		$options = array_merge(['time' => Dates::Now()], $options);

		$i = Dates::GetInterval($options['time']);

		$needs = DB::Query('
			SELECT n.rowid as needid, user_to, name, date, amount,
				(SELECT SUM(amount) FROM payments p WHERE p.needid = n.rowid) AS paid,
				(SELECT COUNT(*)    FROM payments p WHERE p.needid = n.rowid) AS nb_payments,
				(SELECT COUNT(*)    FROM payments p WHERE p.needid = n.rowid AND p.confirm_date IS NOT NULL) AS nb_confirmed_payments
			FROM needs n
			JOIN users u ON u.rowid = n.user_to
			WHERE date >= :thatmonth
			  AND date <  :nextmonth
			  OR  paid IS NULL OR paid != amount
			  ',
			[
				':thatmonth' => $i['thatmonth'],
				':nextmonth' => $i['nextmonth'],
			]
		);

		foreach ($needs as &$n)
		{
			$n['payments'] = Money::GetPayments(['reason'=>'need','needid'=>$n['needid'],'time'=>Dates::ALL]);
			$total_amount = array_sum(array_map(fn($p) => $p['amount'],$n['payments']));
			$n['fulfilled'] = $total_amount == $n['amount'];
			$n['confirmed'] = $total_amount == $n['amount'] && $n['nb_payments'] == $n['nb_confirmed_payments'];
		}

		return $needs;
	}

	public static function AddNeed ($username,$amount)
	{
		self::empty_cache();

		DB::Query(
			'INSERT INTO needs (date, amount, user_to) VALUES (:d, :a, :u)',
			[
				':d' => Dates::Now(),
				':a' => $amount,
				':u' => Users::GetID($username),
			]
		);
	}

	public static function EditNeed ($needid, $amount)
	{
		self::empty_cache();

		// TODO: check that needs exists and is allowed to be modified (= no payments attached)

		if ($amount !== '0' && (int)$amount == 0)  // contribution must be an integer; 0 is also valid
			die('Montant incorrect');

		$amount = (int)$amount;

		if ($amount > 0)
			DB::Query('UPDATE needs SET amount = :a WHERE rowid = :id', [':a' => $amount, ':id' => $needid]);
		else
			DB::Query('DELETE FROM needs WHERE rowid = :id', [':id' => $needid]);
	}

	public static function CreatePayment (int $user_from, int $user_to, int $amount, string $means, string $reason, int $needid=null)
	{
		self::empty_cache();

		if (!in_array($means,['cash','check','transfer','paypal']))
			throw new \Exception("Unknown means of payment '$means'");

		if (!in_array($reason,['redistribution','need']))
			throw new \Exception("Unknown reason for payment '$reason'");

		if ((int)$amount == 0)
			throw new \Exception("Invalid amount '$amount'");

		$time = Dates::Now();

		/* Special case: if redistributions haven't started this month, write a need's payment in last month's accountability.
		 * This way, the need is taken into account before redistribution, and re-balanced.
		 */
		if ($reason == 'need' && count(self::GetPayments(['reason'=>'redistribution'])) == 0)
			$time = Dates::LastDayOfMonth235959('last');

		// consistency checks
		$users = Users::GetAll(['time' => $time]);
		foreach ([$user_from,$user_to] as $userid)
			if (count(array_filter($users, fn($u) => $u['rowid'] == $userid)) == 0)
			{
				$msg = "Unknown userID $userid";

				// if user exists in database, it means they didn't exist back at $time (or won't be a member at $time)
				try
				{
					$user = Users::GetByID($userid);
					$msg = "User {$user['name']} was not yet a member, or is not a member any more, in month " . date('Y-m', $time);
				}
				catch (\Exception $e) {}

				throw new \Exception($msg);
			}

		DB::Query(
			'INSERT INTO payments (date, amount, means, user_from, user_to, confirm_date, why, needid) VALUES (:d, :a, :m, :f, :t, :c, :w, :n)',
			[
				':d' => $time,
				':a' => $amount,
				':m' => $means,
				':f' => $user_from,
				':t' => $user_to,
				':c' => null,  // not confirmed, just created
				':w' => $reason,
				':n' => $needid,
			]);
	}

	public static function DeletePayment ($paymentid)
	{
		self::empty_cache();
		DB::Query('DELETE FROM payments WHERE rowid = :id', [':id' => (int)$paymentid]);
	}

	public static function EditPayment ($paymentid, $amount)
	{
		self::empty_cache();

		if ((int)$amount == 0)
			throw new \Exception("Invalid amount '$amount'");

		DB::Query('UPDATE payments SET amount = :a WHERE rowid = :id', [
			':a'  => $amount,
			':id' => (int)$paymentid,
		]);
	}

	public static function ConfirmPayment ($paymentid, $means)
	{
		self::empty_cache();

		DB::Query('
			UPDATE payments
			SET confirm_date = :time,
				means = :means
			WHERE rowid = :id
			',
			[
				':time' => Dates::Now(),
				':means' => $means,
				':id' => $paymentid,
			]);
	}

	public static function GetPayments($options)
	{
		$ckey = self::ckey(__FUNCTION__, $options);
		$cached = self::get_cache($ckey);
		if (!is_null($cached))
			return $cached;

		$options = array_merge(['time' => Dates::Now()], $options);
		$i = Dates::GetInterval($options['time']);

		$reason = $options['reason'] ?? null;
		if (!in_array($reason,['redistribution','need','all']))
			throw new \Exception('Reason not defined or unknown');

		$query = '
			SELECT p.rowid,
				uf.name AS name_from, ut.name AS name_to, uf.rowid AS id_from, ut.rowid AS id_to,
				p.date, p.amount, IIF(p.user_from = p.user_to, "retrait", means) AS means, confirm_date, needid,
				n.date AS needdate
			FROM payments p
			JOIN users uf ON uf.rowid = p.user_from
			JOIN users ut ON ut.rowid = p.user_to
			LEFT JOIN needs n ON n.rowid = p.needid
			WHERE p.date >= :thatmonth
			  AND p.date <  :nextmonth
	  ';
		$values = [
			':thatmonth' => $i['thatmonth'],
			':nextmonth' => $i['nextmonth'],
		];

		if ($reason != 'all'){
			$query .= ' AND why = :why ';
			$values[':why'] = $reason;
		}

		if ($options['needid'] ?? false)
		{
			$query .= ' AND needid = :needid ';
			$values[':needid'] = $options['needid'];
		}

		if ($options['from_or_to'] ?? false)
		{
			$query .= ' AND (p.user_to = :user_to OR p.user_from = :user_from) ';
			$values[':user_to'] = Users::GetID($options['from_or_to']);
			$values[':user_from'] = Users::GetID($options['from_or_to']);
		}

		if (isset($options['confirmed']))
			$query .= ' AND confirm_date ' . ($options['confirmed'] ? 'IS NOT NULL' : 'IS NULL');

		return self::set_cache($ckey, DB::Query($query,$values));
	}

	public static function GetEquilibrium ($options=[])
	{
		$options = array_merge(['time' => Dates::Now()], $options);

		$users = Users::GetAll(['time'=>$options['time']]);
		if (empty($users))
			return [];

		$contrib = self::GetContributions($options);
		$redistr_payments = self::GetPayments(array_merge($options,['reason'=>'redistribution']));
		$needs_payments = self::GetPayments(array_merge($options,['reason'=>'need']));
		$contrib_totals = self::GetContributionsTotals($options);

		if ($options['time'] < DB::GetOldestDataDate())
			return [];

		foreach ($users as &$user)
		{
			$user['contrib'] = array_filter($contrib, fn($c) => $c['userid'] == $user['rowid']);
			$user['contrib'] = count($user['contrib']) > 0 ? round(array_values($user['contrib'])[0]['amount']) : 0;
			$user['diff_avg_contrib'] = max(0, $contrib_totals['avg_contrib'] - $user['contrib']);
		}
		unset($user);

		// exclude users who don't participate this month
		$participate = array_map(fn($c) => $c['userid'], array_filter($contrib, fn($c) => $c['participates']));
		$participating_users = array_filter($users, fn($u) => in_array($u['rowid'], $participate));

		$total_diff_contrib = array_sum(array_map(fn($u) => $u['diff_avg_contrib'], $participating_users));

		$prev_month = strtotime(date('Y-m-01',$options['time']) . '-1 month');
		$prev_registers = self::GetRegisters(['time' => $prev_month, 'users' => $users]);
		$prev_register_avg = array_sum(array_map(fn($r) => $r['register'], $prev_registers)) / count($prev_registers);

		$equil = [];
		foreach ($users as $user)
		{
			$userid = $user['rowid'];
			$participates = in_array($userid, $participate);

			$ratio_diff_contrib = $total_diff_contrib ? $user['diff_avg_contrib'] / $total_diff_contrib : 0;

			$payments = [
				'pay_redistr' => array_filter($redistr_payments, fn($p) => $p['id_from'] == $userid),
				'pay_needs'   => array_filter($needs_payments,   fn($p) => $p['id_from'] == $userid),
				'receive'     => array_filter($redistr_payments, fn($p) => $p['id_to']   == $userid),
			];
			foreach ($payments as $op => &$pp)
				$pp = array_map(fn($p) => $p['amount'], $pp);
			unset($pp);

			$user_redistr = round($contrib_totals['sum_contrib_redistr'] * $ratio_diff_contrib);

			$prev_register = array_filter($prev_registers, fn($c) => $c['userid'] == $userid);
			$prev_register = empty($prev_register) ? 0 : array_values($prev_register)[0]['register'];
			$diff_register = $prev_register_avg - $prev_register;

			$user_needs = $user_redistr - $user['contrib'] + $contrib_totals['avg_contrib_redistr'] + $diff_register;
			$user_equil = -$user_needs - array_sum($payments['pay_redistr']) + array_sum($payments['receive']);

			$user_register_no_needs = $prev_register + $user['contrib'] - $user_redistr - array_sum($payments['pay_redistr']) + array_sum($payments['receive']);
			$user_register = ($participates ? $user_register_no_needs : $prev_register)  - array_sum($payments['pay_needs']);

			$equil[$userid] = [
				'name' => $user['name'],
				'participates' => $participates,
				'needs' => $user_needs,
				'equil' => $user_equil,
				'contrib' => $user['contrib'],
				'diff_avg_contrib' => $user['diff_avg_contrib'],
				'ratio_diff_contrib' => $ratio_diff_contrib,
				'redistr' => $user_redistr,
				'prev_register' => $prev_register,
				'diff_register' => $diff_register,
				'payments' => $payments,
				'register' => $user_register,
				'register_no_needs' => $user_register_no_needs,

				// data non dependent on user
				'total_diff_contrib' => $total_diff_contrib,
				'prev_register_avg' => $prev_register_avg,
			];

			foreach ($equil[$userid] as $k => &$v)
				if (!$participates && !in_array($k, ['name', 'participates', 'register', 'prev_register']))
					$v = 0;
			unset($v);
		}

		return $equil;
	}

	// TODO: replace by redistribution payments freeze, decided by accountants
	public static function GetEquilibriumLeftToPay ($options=[])
	{
		$options = array_merge(['time' => Dates::Now()], $options);
		$equil = self::GetEquilibrium($options);
		return array_sum(array_map(fn($e) => $e['equil'] > 0 ? $e['equil'] : 0, $equil));
	}

	public static function GetDBRegisters($date)
	{
		$date = Dates::LastDayOfMonth235959($date);
		$ckey = self::ckey(__FUNCTION__, ["date" => $date]);

		$cached = self::get_cache($ckey);
		if (!is_null($cached))
			return $cached;

		$res = DB::Query('
			SELECT user, amount
			FROM registers
			WHERE date = :date
			',
			[':date' => $date]
		);

		$registers = [];
		foreach ($res as $r)
			$registers[$r['user']] = $r['amount'];

		return self::set_cache($ckey, $registers);
	}

	public static function GetRegisters ($options=[])
	{
		$options = array_merge(['time' => Dates::Now()], $options);

		$users = $options['users'] ?? Users::GetAll(['time'=>$options['time']]);

		return array_map(fn ($u) => [
			'userid' => $u['rowid'],
			'name' => $u['name'],
			'register' => self::GetRegister(array_merge($options,['username'=>$u['name']]))
		], $users);
	}

	public static function GetRegister ($options=[])
	{
		$ckey = self::ckey(__FUNCTION__, $options);
		$cached = self::get_cache($ckey);
		if (!is_null($cached))
			return $cached;

		$options = array_merge(['time' => Dates::Now()], $options);

		$equil = self::GetEquilibrium(['time'=>$options['time']]);
		if (empty($equil))  // e.g. not all contribs declared yet this month
			$equil = self::GetEquilibrium(['time'=>Dates::PrevMonth($options['time'])]);

		$username = $options['username'] ?? null;
		if (!is_null($username))
			$equil = array_filter($equil, fn($e) => $e['name'] == $username);
		else
		{
			$usernames_at_date = array_map(fn($u) => $u['name'], Users::GetAll(['time'=>$options['time']]));
			$equil = array_filter($equil, fn($e) => in_array($e['name'], $usernames_at_date));
		}

		// override registers with existing ones in DB
		$db_registers = self::GetDBRegisters($options['time']);

		foreach($equil as $userid => &$e)
			$e['register'] = $db_registers[$userid] ?? $e['register'];
		unset($e);

		foreach($db_registers as $userid => $amount)
			if (!isset($equil[$userid]) && (is_null($username) || $userid == Users::GetID($username)))
				$equil[$userid] = ['register' => $amount];

		return self::set_cache($ckey, array_sum(array_map(fn($e) => $e['register'], $equil)));
	}

	public static function CloseContributions() {
		foreach(self::WhoDidntDeclare() as $user_name)
			Money::DeclareContribution($user_name, participates: false);
	}

	private static function payment_csv_line($out,$users,$payment)
	{
		$from_index = array_keys(array_filter($users, fn($u) => $u['name'] == $payment['name_from']))[0];
		$to_index   = array_keys(array_filter($users, fn($u) => $u['name'] == $payment['name_to'])  )[0];
		$line = array_fill(0,count($users),'');
		$line[$to_index]   = +$payment['amount'];
		$line[$from_index] = -$payment['amount'];
		$label = 'Redistribution';
		if ($payment['needid'])
			$label = $from_index == $to_index ? "Retrait {$payment["name_to"]}" : 'Besoin';
		fputcsv($out,array_merge([$label], $line, [$label == "Besoin" ? "{$payment['name_from']} → {$payment["name_to"]}" : '']));
	}

	public static function Export ($month)
	{
		$time = strtotime("$month-01");

		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=$month.csv");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");

		$out = fopen('php://output','w');

		// users
		$users = Users::GetAll(['time'=>$time]);
		fputcsv($out,array_merge(['Membre'],array_map(fn($u) => $u['name'], $users),['Total']));

		// prev month registers
		$registers = array_map(fn($r) => $r['register'], self::GetRegisters(['time'=>Dates::PrevMonth($time)]));
		fputcsv($out,array_merge(['Report mois précédent'],$registers,[array_sum($registers)]));

		// redistributions
		$equil = self::GetEquilibrium(['time'=>$time]);
		$redistr = array_map(fn($e) => $e['redistr'] ? -$e['redistr'] : 0, $equil);
		fputcsv($out,array_merge(['Redistribution'], $redistr, [-array_sum($redistr)]));

		// contributions
		$contribs = self::GetContributions(['time'=>$time]);
		$contribs = array_map(fn($c) => $c['amount'], $contribs);
		fputcsv($out,array_merge(['Cotisation'], $contribs, [array_sum($contribs)]));

		// redistributions
		fputcsv($out,[]);
		$redistr = self::GetPayments(['time'=>$time, 'reason'=>'redistribution']);
		foreach ($redistr as $payment)
			self::payment_csv_line($out,$users,$payment);

		$registers_no_needs = array_map(fn($e) => $e['register_no_needs'], $equil);
		fputcsv($out,array_merge(['Caisses après redistribution'],$registers_no_needs,[array_sum($registers_no_needs)]));
		fputcsv($out,[]);

		$needs = self::GetPayments(['time'=>$time, 'reason'=>'need']);
		foreach ($needs as $payment)
			self::payment_csv_line($out,$users,$payment);

		// month end registers
		$registers = array_map(fn($r) => $r['register'], self::GetRegisters(['time'=>$time]));
		fputcsv($out,array_merge(['Caisses fin de mois'],$registers,[array_sum($registers)]));

		fclose($out);
		die();
	}

	public static function HandlePaymentAction ($redirect_to)
	{
		$action = $_POST['action'] ?? null;
		switch ($action)
		{
			case 'ask':
				@session_start();
				Money::AddNeed($_SESSION['user']['name'], $_POST['amount']);
				header("Location: $redirect_to");
				die();
				break;

			case 'add':
				Money::CreatePayment($_POST['user_from'], $_POST['user_to'], $_POST['amount'], $_POST['means'], $_POST['reason'], $_POST['needid']?:null);
				header("Location: $redirect_to");
				die();
				break;

			case 'confirm':
				Money::ConfirmPayment($_POST['paymentid'], $_POST['means']);
				header("Location: $redirect_to");
				die();
				break;

			case 'delete':
				Money::DeletePayment($_POST['paymentid']);
				header("Location: $redirect_to");
				die();
				break;

			case 'edit':
				Money::EditPayment($_POST['paymentid'], $_POST['amount']);
				header("Location: $redirect_to");
				die();
				break;
		}
	}

	public static function HandleNeedAction ($redirect_to)
	{
		$action = $_POST['action'] ?? null;
		switch ($action)
		{
			case 'edit_need':
				Money::EditNeed($_POST['needid'], $_POST['amount']);
				header("Location: $redirect_to");
				die();
				break;
		}
	}
}
