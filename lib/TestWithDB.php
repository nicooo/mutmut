<?php declare(strict_types=1);

namespace Mutmut;

use PHPUnit\Framework\TestCase;

class TestWithDB extends TestCase
{
	protected function setUp():    void { \Mutmut\DB::SetTesting(true); }
	protected function tearDown(): void { \Mutmut\DB::DeleteTestingDBFile(); }

	protected function assertRoundedEquals($a, $b, $msg = '')
	{
		$this->assertEquals(round($a), round($b), $msg);
	}
}
