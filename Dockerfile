
FROM debian:bookworm

ENV TIMEZONE Europe/Paris
RUN apt update && apt -y upgrade
RUN apt install -y tzdata
RUN apt install -y apache2 libapache2-mod-php8.2
RUN apt install -y php8.2-sqlite3 php8.2-iconv
RUN apt install -y composer
RUN apt clean

WORKDIR /var/www/html

# Install twig via composer
COPY composer.json composer.json
RUN composer update

# Configure timezone
RUN cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime && \
    echo "${TIMEZONE}" > /etc/timezone

# Configure php
RUN sed -i 's/session.cookie_httponly =/session.cookie_httponly = true/' /etc/php/8.2/apache2/php.ini

# Enable mutmut site
COPY docker/apache-site-mutmut.conf /etc/apache2/sites-available/mutmut.conf
RUN a2dissite 000-default
RUN a2enmod rewrite
RUN a2ensite mutmut.conf

# Copy project files
ADD htdocs htdocs
ADD lib    lib
ADD twig   twig
RUN mkdir  data && \
    rm -rf twig/cache && \
    mkdir  twig/cache && \
    chown -R www-data:www-data data twig/cache

ENTRYPOINT ["/usr/sbin/apache2ctl", "-DFOREGROUND"]
