<?php declare(strict_types=1);

namespace Mutmut;

final class GlobalTest extends TestWithDB
{
	public function testAll()
	{
		Dates::SetNow(strtotime('2022-01-13'));

		// admin user in a newly created database
		$this->assertEquals(count(array_filter(Users::GetAll(), fn($u) => $u['name'] == 'admin')), 1);

		// create user
		Users::Create('bob', '', 'passbob1');
		$this->assertEquals(count(Users::GetAll()), 2);

		$adm = Users::GetID('admin');
		$bob = Users::GetID('bob');

		// declare contributions
		Dates::SetNow(strtotime('2022-01-14')); Money::DeclareContribution('admin', 150);
		Dates::SetNow(strtotime('2022-01-15')); Money::DeclareContribution('bob', 50);

		// first redistribution
		$equil = Money::GetEquilibrium();
		$this->assertRoundedEquals($equil[1]['needs'], -100);
		$this->assertRoundedEquals($equil[2]['needs'], +100);

		// balance money
		Dates::SetNow(strtotime('2022-01-16'));
		Money::CreatePayment($adm, $bob, 100, 'cash', 'redistribution');

		$equil = Money::GetEquilibrium();
		$this->assertRoundedEquals($equil[$adm]['equil'], 0);
		$this->assertRoundedEquals($equil[$bob]['equil'], 0);

		$registers = Money::GetRegisters();
		$this->assertRoundedEquals($registers[0]['register'], 50);
		$this->assertRoundedEquals($registers[1]['register'], 50);

		// someone comes in, they should be set to join next 1st of month
		Users::Create('camelia', '', 'magnolia78');

		$this->assertEquals(Users::GetbyName('camelia')['since'], strtotime('2022-02-01'), "Redistribution has happened; user should only join next month");

		$cam = Users::GetID('camelia');

		Dates::SetNow(strtotime('2022-02-02')); Money::DeclareContribution('admin', 100);
		Dates::SetNow(strtotime('2022-02-05')); Money::DeclareContribution('bob', 100);
		Dates::SetNow(strtotime('2022-02-06')); Money::DeclareContribution('camelia', 200);

		// balance money, 2nd redistribution
		Dates::SetNow(strtotime('2022-02-09'));
		Money::CreatePayment($cam, $adm, 50, 'cash', 'redistribution');
		Money::CreatePayment($cam, $bob, 50, 'cash', 'redistribution');

		$equil = Money::GetEquilibrium();
		$this->assertRoundedEquals($equil[$adm]['equil'], 0);
		$this->assertRoundedEquals($equil[$bob]['equil'], 0);
		$this->assertRoundedEquals($equil[$cam]['equil'], 0);

		$registers = Money::GetRegisters();
		$this->assertRoundedEquals($registers[0]['register'], 100);
		$this->assertRoundedEquals($registers[1]['register'], 100);
		$this->assertRoundedEquals($registers[2]['register'], 100);

		// 3rd redistribution
		// Test closing contributions when not everyone has declared
		Dates::SetNow(strtotime('2022-03-03')); Money::DeclareContribution('bob', '0');
		Dates::SetNow(strtotime('2022-03-04')); Money::DeclareContribution('camelia', 200);
		Money::CloseContributions();

		$equil = Money::GetEquilibrium();
		$this->assertRoundedEquals($equil[$adm]['equil'], 0, "There should not be any redistribution for admin, who's not participating this month");
		$this->assertRoundedEquals($equil[$bob]['equil'], -150);
		$this->assertRoundedEquals($equil[$cam]['equil'], 150);

		Dates::SetNow(strtotime('2022-03-09'));
		Money::CreatePayment($cam, $bob, 150, 'cash', 'redistribution');

		$registers = Money::GetRegisters();

		$this->assertRoundedEquals($registers[0]['register'], 100, "admin's register must stay the same when not participating in a month's redistributions");
		$this->assertRoundedEquals($registers[1]['register'], 150);
		$this->assertRoundedEquals($registers[2]['register'], 150);

		/* TODO new month
		 * 1. empty registers by withdrawing money for all 3 users
		 * 2. check that they are
		 * 3. ¿redistribute and check that everybody takes part?
		 */
	}
}
